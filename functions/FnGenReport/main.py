# -*- coding: utf-8 -*-
import os
import requests
import ftplib
import datetime
import time
import json
import sqlite3
import calendar
import xlsxwriter
import boto3
snsclient = boto3.client(
    "sns",
    region_name=os.environ['AWS_REGION']
)

def handle(event,context):
    try:
        body = json.loads(event['body'])
        if 'timezone' in body.keys():
            t = body['timezone']
        else:
            t = 7
        if 'smslist' in body.keys():
            smslist = body['smslist']
        else:
            smslist = []
        iden = body['id']
        dateye = body['year']
        datemo = body['month']
        maillist = body['maillist']
        dbfilename = ftpMikrotik2Lambda(body['ftphost'],body['ftpuser'],body['ftppass'],body['ftppath'])
        objreport = dbquery(dbfilename,iden,dateye,datemo,t)
        reportname,num = genreport(objreport,iden,dateye,datemo,t)
        print reportname,num
        mailsubject = "#%s/%s %s Monthly Report"%(dateye,datemo,iden)
        mailtext = "%s/%s %s Monthly Report \nThis report generated by AWS Lambda function."%(dateye,datemo,iden)
        mailtext +="\nGenerated Time : %s"%(datetime.datetime.utcfromtimestamp(time.time()+(t*3600)).strftime('%Y-%m-%d %H:%M:%S'))
        print send_mail(maillist,mailsubject,mailtext,reportname)

        smsmsg = u'รายงานการขายบัตรอินเตอร์เน็ต '+str(iden)+' ประจำเดือน '+str(datemo)+'/'+str(dateye)+u' จำนวน '+str(num)+u' ใบ'
        for PhoneNumber in smslist:
            print send_sms(PhoneNumber,smsmsg)
        return {'statusCode': 200,'body': json.dumps({"message":"ok"})}
    except Exception as e: 
        print e
        return {'statusCode': 500,'body': json.dumps({"message":"error"})}

def ftpMikrotik2Lambda(host,username,password,remotePath="/user-manager/sqldb",port=21):
    try:
        ftp = ftplib.FTP(host)
        ftp.login(username,password)
        filename = '/tmp/'+str(time.time())
        
        ftp.retrbinary("RETR " + remotePath ,open(filename, 'wb').write)
        ftp.quit()
        return filename
    except:
        print "Error"

def dbquery(dbfilename,iden,ye,mo,timezone=7):
    offsettime = (timezone*-1)*3600
    
    _,ld = calendar.monthrange(int(ye),int(mo))
    range1s = "01/%s/%s"%(mo,ye)
    range2s = "%s/%s/%s-23:59:59"%(ld,mo,ye)
    range1 = time.mktime(datetime.datetime.strptime(range1s, "%d/%m/%Y").timetuple())+offsettime
    range2 = time.mktime(datetime.datetime.strptime(range2s, "%d/%m/%Y-%H:%M:%S").timetuple())+offsettime
    sql = """select ses.userId ,ses.fromTime as fromtime ,user.userName as username from 
	(Select userId , min(fromTime) as fromTime from session group by userId) 
        as ses left join user on ses.userId = user.id
        WHERE fromtime BETWEEN ? AND ? order by fromTime """
    conn = sqlite3.connect(dbfilename)
    c = conn.cursor()
    params = (range1,range2)
    return c.execute(sql,params)

def genreport(objreport,iden,ye,mo,timezone=7):
    offsettime = (timezone)*3600
    try:
        filename = "/tmp/%s-%s_%sMonthlyReport.xlsx"%(ye,mo,iden)
        workbook = xlsxwriter.Workbook(filename)
        worksheet = workbook.add_worksheet()
        worksheet.set_paper(9)
        rowtable = workbook.add_format()   
        rowtable.set_right(1)
        rowtable.set_left(1)
        endrowtable = workbook.add_format()   
        endrowtable.set_right(1)
        endrowtable.set_left(1)
        endrowtable.set_bottom(1)
        headtable = workbook.add_format({'bold': True})
        headtable.set_border(1)
        worksheet.write('A1', 'No.', headtable)
        worksheet.set_column('B:B', 25)
        worksheet.write('B1', u'บัตร', headtable)
        worksheet.set_column('C:C', 40)
        worksheet.write('C1', u'วันเวลาที่เริ่มใช้', headtable)
        num = 1
        for i in objreport:
            worksheet.write_string(num,0, str(num),rowtable)
            worksheet.write_string(num,1,str(i[2]),rowtable)
            worksheet.write_string(num,2,str(datetime.datetime.utcfromtimestamp(i[1]+offsettime).strftime('%Y-%m-%d %H:%M:%S')),rowtable)
            num+=1
        worksheet.set_header(u'&U&16รายงานการขาย บัตรอินเตอร์เน็ต ประจำเดือน '+str(mo)+'/'+str(ye)+u' จำนวน '+str(num-1)+u' ใบ')
        worksheet.write_string(num,0," ",endrowtable)
        worksheet.write_string(num,1," ",endrowtable)
        worksheet.write_string(num,2," ",endrowtable)
        print("Write xls successfully")
    except:
        print("Error: unable to write xls")
    finally:
        workbook.close()
        
    return (filename,num-1)

def send_mail(to, subject, text, attach):
    mailgundomain   = 'mikrotikeasy.com'
    mailgunapikey   = 'key-ff38b5bc8b7a2f7eebe922b5b7089efe'
    mailgunfrom     = 'Reports System <postmaster@mikrotikeasy.com>'
    return requests.post(
        "https://api.mailgun.net/v3/%s/messages" % mailgundomain,
        auth=("api", mailgunapikey),
        files=[("attachment", open(attach))],
        data={"from": mailgunfrom,
              "to": to,
              "subject": subject,
              "text": text})
    
def send_sms(to, text):
    response = snsclient.publish(
        PhoneNumber=to,
        Message=text
    )
    return response
