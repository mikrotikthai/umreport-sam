:local routerip "172.21.10.9"
:local user "mikrotikthai"
:local pass "mikrotikthai!"
:local path "/user-manager/sqldb"
:local apiendpoint "https://ik6dgnxitd.execute-api.ap-southeast-1.amazonaws.com/v1/gen"
#datetonum#
:local date [/system clock get date]
:local identity [/system identity get name]
:local ye
:local mo
:local input 
:set input [:toarray $date]
:if ([:len $input] > 0) do={
	:local input1 [:tostr [:pick $input 0]]
	:set ye ([:pick $input1 7 11])
	:local months [:toarray "jan,feb,mar,apr,may,jun,jul,aug,sep,oct,nov,dec"]
	:for x from=0 to=([:len $months] - 1) do={
		:if ([:tostr [:pick $months $x]] = [:tostr [:pick $input1 0 3]]) do={
			:if ($x = 0) do={ :set mo 12; :set ye ($ye-1);  } else={ :set mo $x } 
			:if ($x < 9) do={ :set mo ("0" . ($mo)) }
		} 
	}
}
#datetonum#

:if (date~"/01/") do={
#place instructions here
	:log info "start gen monthly report"
	/tool fetch http-method=post http-content-type="application/json" url="$apiendpoint" http-data="{\"id\":\"$identity\",\"ftphost\":\"$routerip\",\"ftpuser\":\"$user\",\"ftppass\":\"$pass\",\"ftppath\":\"$path\",\"year\":\"$ye\",\"month\":\"$mo\",\"maillist\":[\"phai.namikaze@gmail.com\",\"engineer.mikrotikthai@gmail.com\",\"mikrotikthai@gmail.com\"],\"smslist\":[\"+66843288435\",\"+66972395585\",\"+66971251480\"],\"timezone\":7}"
	:log info "end gen monthly report"
};