**umreport-sam**

---

## deploy stack
1. package
aws cloudformation package --template-file template.yaml --s3-bucket mikrotikthai --s3-prefix umreport-sam --output-template-file packaged-template.yaml

2. deploy
aws cloudformation deploy --template-file D:\project\umreport-sam\packaged-template.yaml --stack-name umreport-sam --capabilities CAPABILITY_NAMED_IAM